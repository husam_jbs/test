/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author H.Almasri
 */
public class Main {

    public String printName(String name) {
        return name;
    }

    public int sumNum(int x, int y) {
        return x + y;
    }

    public int subNum(int x, int y) {
        return x - y;
    }

    public static void main(String[] args) {
        Main main = new Main();
        System.out.println(main.printName("Husam"));
        System.out.println(main.sumNum(2, 2));
        System.out.println(main.subNum(2, 2));
    }
}
